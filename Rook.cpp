/**
 * Trisha Karani tkarani1
 * Jeanie Fung jfung4
 * Roxana Leal Toledo rlealto1
 */

#include "Rook.h"

bool Rook::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  
    if ((start.first == end.first) || (start.second == end.second)) {
        return true;
    }
    return false;

}
