// Pawn.cpp
/*
 * Trisha Karani tkarani1
 * Jeanie Fung jfung4
 * Roxana Leal rlealto1
 */

#include "Pawn.h"

bool Pawn::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
    int row_diff = end.second - start.second; 
    if (start.first != end.first) { //false if not in same column
        return false; 
    }

    if(is_white()) {
        if(row_diff == 1) { //white can only move up 
	    return true; 
	}

	else if (row_diff == 2 && start.second == '2'){ //can only move by 2 in start position
            return true; 
	}
    }

    else {
        if(row_diff == -1) { //black can only move down
            return true; 
	}

	else if ((row_diff == -2) && start.second == '7') { //can only move by 2 in start position
	    return true; 
	}
    }

    return false; 
}


bool Pawn::legal_capture_shape(std::pair<char, char> start, std::pair<char, char> end) const {
    int row_diff = end.second - start.second;
    int col_diff = end.first - start.first;

    if (col_diff != 1 && col_diff != -1) { //can move to the right or left row 
        return false;
    }

    if (is_white()) {
        if (row_diff != 1) { //white can only move up 
            return false;
	}
    }
    else {
	if (row_diff != -1) { //black can only move down
            return false;
	}
    }

    return true;
}


