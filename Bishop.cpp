// Bishop.cpp
/*Trisha Karani - tkarani1
 *Jeanie Fung - jfung4
 *Roxana Leal Toledo - rlealto1
*/
#include "Bishop.h"
#include <stdlib.h>

bool Bishop::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {

    int col_diff = abs((int) (end.first - start.first));
    int row_diff = abs((int) (end.second - start.second));
    
    if (col_diff == row_diff) {
        return true;
    }
    
    return false;
    
}
