// Chess.h
/*Trisha Karani - tkarani1
 *Jeanie Fung - jfung4
 *Roxana Leal Toledo - rlealto1
*/

#ifndef CHESS_H
#define CHESS_H

#include <iostream>
#include "Piece.h"
#include "Board.h"

class Chess {

public:

	// This default constructor initializes a board with the standard
	// piece positions, and sets the state to white's turn
	Chess();

	//copy constructor
	Chess(const Chess& copy);

	// Returns a constant reference to the board 
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	const Board& get_board() const { return board; }

	// Returns true if it is white's turn
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	bool turn_white() const { return is_white_turn; }

	// Attemps to make a move. If successful, the move is made and
	// the turn is switched white <-> black
	bool make_move(std::pair<char, char> start, std::pair<char, char> end);

	// Returns true if the designated player is in check
	bool in_check(bool white) const;

	// Returns true if the designated player is in mate
	bool in_mate(bool white) const;

	// Returns true if the designated player is in mate
	bool in_stalemate(bool white) const;

    //Returns a non const board that allows editing
	Board& get_edit_board() { return board; }

    //Sets is_white_turn to true or false depending of character
    //provided.
    void set_turn(char color);

private:
	// The board
	Board board;

	// Is it white's turn?
	bool is_white_turn; //originally bool

    // Returns true if the capture movement is legal and there is a valid piece to
    // caputre, false otherwise.
    bool can_capture(std::pair<char, char> start, std::pair<char, char> end) const;

    // Returns true if the attempted movement is of legal shape, it is the turn of the
    // given piece's color team and there are no pieces in the way.
    bool can_move(std::pair<char, char> start, std::pair<char, char> end) const;
};

// Writes the board out to a stream
std::ostream& operator<< (std::ostream& os, const Chess& chess);

// Reads the board in from a stream
std::istream& operator>> (std::istream& is, Chess& chess);


#endif // CHESS_H
