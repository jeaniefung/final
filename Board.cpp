// Board.cpp
/* Trisha Karani - tkarani1
 * Jeanie Fung - jfung4
 * Roxana Leal Toledo - rlealto1
*/
#include <iostream>
#include <utility>
#include <map>
#include <string>
#include "Board.h"
#include "CreatePiece.h"
#include <stdlib.h>
#include <vector>
#include "Terminal.h"

using std::make_pair;
using std::pair;
using std::vector; 
using std::map; 

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Board::Board() {}

Board::Board(const Board &orig_board){
    std::map<std::pair<char, char>, Piece*> occ;
    pair <char, char> position;
    char piece_type; 
    for (map<pair<char,char>, Piece*>::const_iterator it = orig_board.occ.begin();
        it != orig_board.occ.end(); ++it) {
        position = it->first; 
        piece_type = it->second->to_ascii();
        add_piece(position, piece_type); 
  }
}


void copy_board(Board& to_copy, Board& to_fill){
    to_fill.clear_board(); 
    pair <char, char> position;
    char piece_type; 
    for (map<pair<char,char>, Piece*>::iterator it = to_copy.occ.begin();
          it != to_copy.occ.end(); ++it) {
        position = it->first; 
        piece_type = it->second->to_ascii();
        to_fill.add_piece(position, piece_type); 
    }
}

Board::~Board() {
    clear_board(); 
}

void Board::clear_board(){
    for (map<pair<char,char>, Piece*>::iterator it = occ.begin(); it != occ.end(); ++it) {
        delete it->second;
    }
    occ.clear(); 
}

const Piece* Board::operator()(std::pair<char, char> position) const {
    if (occ.find(position) == occ.end())
	return NULL;
    
    return occ.at(position);
}

bool Board::add_piece(std::pair<char, char> position, char piece_designator) {
    occ[position] = create_piece(piece_designator);
    return true;
}


vector<pair<char,char>> Board::all_color_pieces(bool white) const {
    vector<pair <char, char>> all_pieces; 
    for (map<pair<char,char>, Piece*>::const_iterator it = occ.begin(); 
         it != occ.end(); ++it) {
        if (it->second->is_white() == white) {
	    all_pieces.push_back(it->first); 
        }
    }

    return all_pieces; 
}
/*Checks for default condition of get_king_position*/
bool Board::has_valid_kings() const{
    pair<char,char> white_king = get_king_position(true); 
    pair<char,char> black_king = get_king_position(false); 

    if (white_king.first == '0' || black_king.first == '0') {
        return false; 
    }

    return true; 
}

pair<char, char> Board::get_king_position(bool white) const {
    char king_type; 
    if (white) { 
        king_type = 'K'; 
    }
    else { 
        king_type = 'k';
    }
    
    for (map<pair<char,char>, Piece*>::const_iterator it = occ.cbegin(); it != occ.cend(); ++it) {
        if (it->second->to_ascii() == king_type) {
            return  it->first; 
        }
    }
    return make_pair('0', '0'); //default condition
} 

bool Board::is_piece_promotion(std::pair<char,char> end){
    Piece* piece = occ.at(end); 
    if (piece->to_ascii() == 'p' && end.second == '1') { 
        occ.erase(end); 
        delete piece; 
        add_piece(end, 'q'); 
        return true; 
    } else if (piece->to_ascii() == 'P' && end.second == '8') { 
        occ.erase(end);
        delete piece; 
        add_piece(end, 'Q');
        return true; 
    }
    return false; 
}
/*
 * Checks if any elements exist between the start and end positions. 
 */

bool Board::path_clear(std::pair<char, char> start, std::pair<char, char> end) const {
    int col_diff = abs( (int) (end.first - start.first));
    int row_diff = abs((int) (end.second - start.second));	
    
    //L shape - Knight does not need clear path
    if ((col_diff == 2 && row_diff == 1) || (col_diff == 1 && row_diff == 2)) {
        return true;
    }

    //moving by 1 in any direction, end is already checked in Chess.cpp
    if (col_diff <= 1 && row_diff <= 1) {
        return true;
    }

    //moving vertically
    if (col_diff == 0) {
        return is_vertical_clear(start,end); 
    }

    //moving horizontally
    if (row_diff == 0) {
        return is_horiz_clear(start,end); 
    }

    //moving diagonally
    if (col_diff > 0 && row_diff > 0) {
        return is_diag_clear(start,end); 
    }
   
    //if not moving in a predefined way
    return false;
}

bool Board::is_vertical_clear(std::pair<char, char> start, std::pair<char, char> end) const {
    char lower_row, upper_row; 

    if (start.second < end.second) {
        lower_row = start.second;
        upper_row = end.second;
    } else {
        lower_row = end.second;
        upper_row = start.second;
    } for (char r = lower_row + 1; r < upper_row; r++){
        pair<char, char> test_position = make_pair(start.first, r);
        if (occ.find(test_position) != occ.end()) {
            return false;
        }
    }
    return true;
}

bool Board::is_horiz_clear(std::pair<char, char> start, std::pair<char, char> end) const { 
   
    char lower_col, upper_col; 

    if (start.first < end.first) {
        lower_col = start.first;
        upper_col = end.first;
    } else {
        lower_col = end.first;
        upper_col = start.first;
    } for (char c = lower_col + 1; c < upper_col; c++) {
        pair<char, char> test_position = make_pair(c, start.second);
        if (occ.find(test_position) != occ.end()) {
            return false;
        }
    }
    return true;
}

bool Board::is_diag_clear(std::pair<char, char> start, std::pair<char, char> end) const {
    int raw_col_diff = end.first - start.first;
    int raw_row_diff = end.second - start.second;

    if (raw_col_diff > 0 && raw_row_diff > 0) { // NE diagonal
        for (char c = start.first + 1 , r = start.second + 1; c < end.first ; c++, r++) {
            pair<char,char> test_position = make_pair(c, r);

            if (occ.find(test_position) != occ.end()) { return false; }
        }
    }

    else if (raw_col_diff < 0 && raw_row_diff > 0) { // NW diagonal
        for (char c = start.first - 1 , r = start.second + 1; r < end.second; c--, r++) {
            pair<char,char> test_position = make_pair(c, r);
            
            if (occ.find(test_position) != occ.end()) { return false; }
        }
    }


    else if (raw_col_diff < 0 && raw_row_diff < 0) { // SW diagonal
        for (char c = start.first - 1 , r = start.second - 1; c > end.first ; c--, r--) {
            pair<char,char> test_position = make_pair(c, r);
            
            if (occ.find(test_position) != occ.end()) { return false; }
        }
    }

    else if (raw_col_diff > 0 && raw_row_diff < 0) { // SE diagonal
        for (char c = start.first + 1 , r = start.second - 1; c < end.first ; c++, r--) {
            pair<char,char> test_position = make_pair(c, r);
            
            if (occ.find(test_position) != occ.end()) { return false; }
        }
    }

    return true;

}

void Board::move_piece(std::pair<char, char> start, std::pair<char, char> end) {
    Piece *end_piece = occ[end];
        
    occ.at(end) = occ.at(start);
    occ.erase(start);
    if (end_piece) { //delete end piece if not empty
        delete end_piece;
    }
}

void Board::display() const {

    Board board = *this;
    Terminal::set_default();
    bool white_square = true;
    std::pair <char, char> position;
	
	  
    for (char r = '8'; r >= '1'; r--) {
        std::cout << "\u001b[7m";
        std::cout << " " << r << " ";
        Terminal::set_default();
        for (char c = 'A'; c <= 'H'; c++) {
            position = std::make_pair(c, r);
            const Piece *piece = board(position);
                
            if (piece) {
                if (white_square) {             
                    Terminal::color_bg(Terminal::WHITE);
                    Terminal::color_fg(false, Terminal::WHITE);
                    std::cout << "|";

                    Terminal::color_fg(false, Terminal::BLACK);
                    std::cout << to_unicode(piece);
                    
                    Terminal::color_fg(false, Terminal::WHITE);
                    std::cout << "|";
                    Terminal::set_default();
                } else {
                    std::cout << "\u001b[48;5;95m";
                    std::cout << "\u001b[38;5;95m";
                    // Terminal::color_bg(Terminal::YELLOW);
                    //Terminal::color_fg(false, Terminal::YELLOW);
                    std::cout << "|";

                    Terminal::color_fg(false, Terminal::BLACK);
                    std::cout << to_unicode(piece);

                    std::cout << "\u001b[38;5;95m";
                    //Terminal::color_fg(false, Terminal::YELLOW);
                    std::cout << "|";
                    Terminal::set_default();
                }
            } else {
                if (white_square) {
                    Terminal::color_bg(Terminal::WHITE);
                    Terminal::color_fg(false, Terminal::WHITE);
                    std::cout << "|";
                    std::cout << " ";
                    std::cout << "|";
                    Terminal::set_default();
                } else {
                    std::cout << "\u001b[48;5;95m";
                    std::cout << "\u001b[38;5;95m";
                    //Terminal::color_bg(Terminal::YELLOW);
                    //Terminal::color_fg(false, Terminal::YELLOW);
                    std::cout << "|";
                    std::cout << " ";
                    std::cout << "|";
                    Terminal::set_default();
                }
            }
            white_square = !white_square;
        }
        std::cout << std::endl;
        white_square = !white_square;
    }
    Terminal::color_bg(Terminal::WHITE);
    std::cout << "  ";
    Terminal::set_default();
	
    for (char c = 'A'; c <= 'H'; c++) {
        std::cout << "\u001b[7m";
	    std::cout << "  " << c;
    }
    std::cout << " " << std::endl;
    Terminal::set_default();  
}

std::string Board::to_unicode(const Piece *piece) const {
    char p = piece->to_ascii();
  
    switch(p) {
        case 'K':
            return "\u2654"; 
        case 'k': 
            return "\u265A";
        case 'Q':
            return "\u2655";
        case 'q':
            return "\u265B";
        case 'R':
            return "\u2656";
        case 'r':
            return "\u265C";
        case 'B':
            return "\u2657";
        case 'b':
            return "\u265D";
        case 'N':
            return "\u2658";
        case 'n':
            return "\u265E";
        case 'P':
            return "\u2659";
        case 'p':
            return "\u265F";
        case 'M': 
            return "\u2667"; 
        case 'm': 
            return "\u2663"; 
        default: 
            return NULL; 
    }    
    
}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream &operator<<(std::ostream &os, const Board &board) {
	for (char r = '8'; r >= '1'; r--) {
		for (char c = 'A'; c <= 'H'; c++) {
			const Piece *piece = board(std::pair<char, char>(c, r));
			if (piece) {
				os << piece->to_ascii();
			} else {
				os << '-';
			}
		}
		os << std::endl;
	}
	return os;
}
