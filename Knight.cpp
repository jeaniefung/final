// Knight.cpp
/*
 * Trisha Karani tkarani1
 * Jeanie Fung jfung4
 * Roxana Leal Toledo rlealto1
 */

#include "Knight.h"
#include <stdlib.h>

bool Knight::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
    int row_diff = end.first - start.first;
    int col_diff = end.second - start.second;
    row_diff = abs(row_diff);
    col_diff = abs(col_diff);

    if ((col_diff == 1 && row_diff ==2) || (col_diff == 2 && row_diff == 1)) {
        return true;
    }

    return false;
}
