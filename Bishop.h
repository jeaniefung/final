// Bishop.h
/*
 * Trisha Karani tkarani1
 * Jeanie Fung jfung4
 * Roxana Toledo rlealto1
 */

#ifndef BISHOP_H
#define BISHOP_H

#include "Piece.h"
#include <stdlib.h>

class Bishop : public Piece {

public:

    // Checks whether the pattern between the starting position and end position is legal for Bishop.
    bool legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const;


	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	char to_ascii() const {
		return is_white() ? 'B' : 'b';
	}

private:
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	Bishop(bool is_white) : Piece(is_white) {}

	friend Piece* create_piece(char piece_designator);
};

#endif // BISHOP_H
