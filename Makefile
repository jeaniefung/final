#Trisha Karani tkarani1
#Jeanie Fung jfung4

CC = g++
CFLAGS = -Wall -Wextra -std=c++11 -pedantic -g 

all: chess

chess: main.o Chess.o CreatePiece.o King.o Queen.o Rook.o Knight.o Bishop.o Pawn.o Board.o
	$(CC) main.o Chess.o CreatePiece.o King.o Queen.o Rook.o Knight.o Bishop.o Pawn.o Board.o -o chess

main.o: main.cpp Chess.h
	$(CC) $(CFLAGS) -c main.cpp

Chess.o: Chess.cpp Chess.h
	$(CC) $(CFLAGS) -c Chess.cpp

Board.o: Board.cpp Board.h Terminal.h
	$(CC) $(CFLAGS) -c Board.cpp

CreatePiece.o: CreatePiece.cpp CreatePiece.h
	$(CC) $(CFLAGS) -c CreatePiece.cpp

King.o: King.cpp King.h Piece.h
	$(CC) $(CFLAGS) -c King.cpp

Queen.o: Queen.cpp Queen.h Piece.h
	$(CC) $(CFLAGS) -c Queen.cpp

Rook.o: Rook.cpp Rook.h Piece.h
	$(CC) $(CFLAGS) -c Rook.cpp

Knight.o: Knight.cpp Knight.h Piece.h
	$(CC) $(CFLAGS) -c Knight.cpp

Bishop.o: Bishop.cpp Bishop.h Piece.h
	$(CC) $(CFLAGS) -c Bishop.cpp

Pawn.o: Pawn.cpp Pawn.h Piece.h
	$(CC) $(CFLAGS) -c Pawn.cpp

clean:
	rm -f *.o chess

