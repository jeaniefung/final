// Queen.h
/*
 * Trisha Karani tkarani1
 * Jeanie Fung jfung4
 * Roxana Toledo rlealto1
 */

#ifndef QUEEN_H
#define QUEEN_H

#include "Piece.h"
#include <cmath>
#include <cstdio> //remove for printf

class Queen : public Piece {
public:

    // Checks the pattern between the starting position and end position is legal for queen
    bool legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const;
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	char to_ascii() const {
		return is_white() ? 'Q' : 'q';
	}

private:
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	Queen(bool is_white) : Piece(is_white) {}

	friend Piece* create_piece(char piece_designator);
};

#endif // QUEEN_H
