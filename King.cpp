// King.cpp
/*
 * Trisha Karani tkarani1
 * Jeanie Fung jfung4
 * Roxana Leal Toledo rlealto1
 */

#include "King.h"
#include <stdlib.h>

bool King::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {
  
    int col_diff = end.first - start.first;
    int row_diff = end.second - start.second;
    col_diff = abs(col_diff);
    row_diff = abs(row_diff);

    if((col_diff == 1 && row_diff == 1) ||
       (col_diff == 1 && row_diff == 0) ||
       (col_diff == 0 && row_diff == 1)) {
        return true;
    }
    
    return false;
}
