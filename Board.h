// Board.h
/* Trisha Karani - tkarani1
 * Jeanie Fung - jfung4
 * Roxana Leal Toledo - rlealto1
*/

#ifndef BOARD_H
#define BOARD_H

#include <iostream>
#include <map>
#include "Piece.h"
#include "Pawn.h"
#include "Rook.h"
#include "Knight.h"
#include "Bishop.h"
#include "Queen.h"
#include "King.h"
#include "Mystery.h"
#include <vector>

class Board {

    // Throughout, we will be accessing board positions using an std::pair<char, char>.
	// The assumption is that the first value is the column with values in
        // {'A','B','C','D','E','F','G','H'} (all caps)
	// and the second is the row, with values in {'1','2','3','4','5','6','7','8'}

public:
	// Default constructor
	Board();

    //copy constructor
	Board(const Board &orig_board);
    
    friend void copy_board(Board& to_copy, Board& to_fill);
	
    //deconstructor
    ~Board(); 

    void clear_board();
	// Returns a const pointer to the piece at a prescribed location if it exists,
    // or nullptr if there is nothing there.
	const Piece* operator() (std::pair<char, char> position) const;

	// Attempts to add a new piece with the specified designator, at the given location.
	// Returns false if:
	// -- the designator is invalid,
	// -- the specified location is not on the board, or
	// -- if the specified location is occupied
	bool add_piece(std::pair<char, char> position, char piece_designator);
 
	//Returns true if the path between start and end is clear
	//Also returns true if path is L shaped or piece is only moving one space
	//Does not check start or end points 
	bool path_clear(std::pair<char,char> start, std::pair<char,char> end) const; 
	
	// Displays the board by printing it to stdout
	void display() const;

	// Returns true if the board has the right number of kings on it
	bool has_valid_kings() const;

    // Returns a pair with the position of the king of the given team.
	std::pair<char,char> get_king_position(bool white) const;

    // Returns a vector of pairs with all the positions of a given team's
    // pieces.
    std::vector<std::pair<char,char>> all_color_pieces(bool white) const;  

    // Moves the piece at the starting position to the end position, deleting
    // the piece that was at the end position if there was one.
	void move_piece(std::pair<char, char> start, std::pair<char, char> end);

    // Determines whether a pawn has reaced the opposite end of the board
    // and promotes it to a queen when it has.
	bool is_piece_promotion(std::pair<char,char> end);

    // Helper method to display(). Uses a piece's ascii value to
    // return its unicode string.
    std::string to_unicode(const Piece *piece) const;
  
private:
	// The sparse map storing the pieces, keyed off locations
	std::map<std::pair<char, char>, Piece*> occ;

    // Helper method to path_clear, checks the path is clear is the vertical direction.
	bool is_vertical_clear(std::pair<char,char> start, std::pair<char,char> end) const;

    // Helper method to path_clear, checks the path is clear is the horizontal direction.
	bool is_horiz_clear(std::pair<char,char> start, std::pair<char,char> end) const;

    // Helper method to path_clear, checks the path is clear is the diagonal direction.
	bool is_diag_clear(std::pair<char,char> start, std::pair<char,char> end) const;

};

// Write the board state to an output stream
std::ostream& operator<< (std::ostream& os, const Board& board);

#endif // BOARD_H
