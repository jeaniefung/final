// Queen.cpp
/**
 * Trisha Karani tkarani1
 * Jeanie Fung jfung4
 * Roxana Toledo rlealto1
 */

#include "Queen.h"
#include <stdlib.h>

bool Queen::legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {

    int col_diff = start.first - end.first;
    int row_diff = start.second - end.second;
    col_diff = abs(col_diff);
    row_diff = abs(row_diff);

    if (col_diff == row_diff) {
        return true;
    }
    if ((start.first == end.first) || (start.second == end.second)) {
        return true;
    }
	 
    return false;

}
      
