// King.h
/* Trisha Karani - tkarani1
 * Jeanie Fung - jfung4
 * Roxana Leal Toledo - rlealto1
*/

#ifndef KING_H
#define KING_H

#include "Piece.h"
#include <stdlib.h>

class King : public Piece {

public:

       // Checks whether the pattern between the starting position and end position is legal for King.
       bool legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const;

	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	char to_ascii() const {
		return is_white() ? 'K' : 'k';
	}

private:
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	King(bool is_white) : Piece(is_white) {}

	friend Piece* create_piece(char piece_designator);
};

#endif // KING_H
