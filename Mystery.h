///////////////////////////////////
// IT IS OK TO MODIFY THIS FILE, //
// YOU WON'T HAND IT IN!!!!!     //
///////////////////////////////////
#ifndef MYSTERY_H
#define MYSTERY_H

#include "Piece.h"

class Mystery : public Piece {

public:
	bool legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const {	
        int col_diff = abs((int) (end.first - start.first));
        int row_diff = abs((int) (end.second - start.second));

        if (col_diff == row_diff) {
            return true;
        }
		return false;
	}

	virtual bool legal_capture_shape(std::pair<char, char> start, std::pair<char, char> end) const {
		int row_diff = end.first - start.first;
        int col_diff = end.second - start.second;
        row_diff = abs(row_diff);
        col_diff = abs(col_diff);

        if ((col_diff == 1 && row_diff ==2) || (col_diff == 2 && row_diff == 1)) {
            return true;
        }

        return false;
	}

	char to_ascii() const {
		return is_white() ? 'M' : 'm';
	}

private:
	Mystery(bool is_white) : Piece(is_white) {}

	friend Piece* create_piece(char piece_designator);
};

#endif // MYSTERY_H
