// Pawn.h
/*Trisha Karani - tkarani1
 *Jeanie Fung - jfung4
 *Roxana Leal Toledo - rlealto1
 */

#ifndef PAWN_H
#define PAWN_H

#include "Piece.h"
#include <iostream>
class Pawn : public Piece {

public:
    //Checks the pattern between the starting position and end position are legal for a pawn.
	bool legal_move_shape(std::pair<char, char> start, std::pair<char, char> end) const;

    // Overrides legal_capture_shape in Piece.h anc checks the pattern between the starting position
    // and end position corresponds to pawn's capture movement.
    virtual bool legal_capture_shape(std::pair<char, char> start, std::pair<char, char> end) const; 
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	char to_ascii() const {
		return is_white() ? 'P' : 'p';
	}

private:
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	Pawn(bool is_white) : Piece(is_white) {}

	friend Piece* create_piece(char piece_designator);
};

#endif // PAWN_H
