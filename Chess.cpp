// Chess.cpp
/*
 * Trisha Karani tkarani1
 * Jeanie Fung jfung4
 * Roxana Toledo rlealto1
 */

#include "Chess.h"
#include <vector>

using std::vector; 
using std::pair; 

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Chess::Chess() : is_white_turn(true) {
    // Add the pawns
    for (int i = 0; i < 8; i++) {
        board.add_piece(std::pair<char, char>('A' + i, '1' + 1), 'P');
	board.add_piece(std::pair<char, char>('A' + i, '1' + 6), 'p');
    }

    // Add the rooks
    board.add_piece(std::pair<char, char>( 'A'+0 , '1'+0 ) , 'R' );
    board.add_piece(std::pair<char, char>( 'A'+7 , '1'+0 ) , 'R' );
    board.add_piece(std::pair<char, char>( 'A'+0 , '1'+7 ) , 'r' );
    board.add_piece(std::pair<char, char>( 'A'+7 , '1'+7 ) , 'r' );

    // Add the knights
    board.add_piece(std::pair<char, char>( 'A'+1 , '1'+0 ) , 'N' );
    board.add_piece(std::pair<char, char>( 'A'+6 , '1'+0 ) , 'N' );
    board.add_piece(std::pair<char, char>( 'A'+1 , '1'+7 ) , 'n' );
    board.add_piece(std::pair<char, char>( 'A'+6 , '1'+7 ) , 'n' );

    // Add the bishops
    board.add_piece(std::pair<char, char>( 'A'+2 , '1'+0 ) , 'B' );
    board.add_piece(std::pair<char, char>( 'A'+5 , '1'+0 ) , 'B' );
    board.add_piece(std::pair<char, char>( 'A'+2 , '1'+7 ) , 'b' );
    board.add_piece(std::pair<char, char>( 'A'+5 , '1'+7 ) , 'b' );

    // Add the kings and queens
    board.add_piece(std::pair<char, char>( 'A'+3 , '1'+0 ) , 'Q' );
    board.add_piece(std::pair<char, char>( 'A'+4 , '1'+0 ) , 'K' );
    board.add_piece(std::pair<char, char>( 'A'+3 , '1'+7 ) , 'q' );
    board.add_piece(std::pair<char, char>( 'A'+4 , '1'+7 ) , 'k' );

}

bool Chess::make_move(std::pair<char, char> start, std::pair<char, char> end) {
    Board temp_board = this->get_board();
   
    if (!can_move(start, end)) { //move must be legal
        return false; 
    }

    board.move_piece(start,end); 
   
    if (in_check(is_white_turn)) { //movement cannot result in checked status
        copy_board(temp_board, board);
        return false; 
    }
   
    board.is_piece_promotion(end); //promote pawn if necessary 
    is_white_turn = !is_white_turn; //move to next player
    return true; 
}


bool Chess::can_move(std::pair<char, char> start, std::pair<char, char> end) const {

    const Piece* start_piece = board(start);
    const Piece* end_piece = board(end);

    if (!start_piece) { //check for valid start position
        return false; 
    }
    if (start_piece == end_piece) { // check if no move
        return false; 
    }
    if (start_piece->is_white() != is_white_turn) { //check if piece and turn color are same 
	return false; 
    } 
    if (end.first < 'A' || end.first > 'H' ||  end.second < '1' || end.second > '8') { //check bounds 
	return false; 
    } 

    //in regular move, end piece is NULL 
    if (end_piece == NULL) {
        if (!start_piece->legal_move_shape(start, end)) { //check if move shape is legal
	    return false; 
	}
	if (!board.path_clear(start, end)) {//check if path is clear 
	    return false; 
	}
    }

    //in capturing move, end piece is not NULL 
    else {
        return can_capture(start, end); 
    }

    //if all conditions are met, piece can be moved 
    return true;

}

/*
 *This function is only in instances where the start and end positions are not equal and if the end position is valid and occupied. 
 */
bool Chess::can_capture(std::pair<char, char> start, std::pair<char, char> end) const {

    const Piece* start_piece = board(start);
    const Piece* end_piece = board(end);

    if (end_piece->is_white() == start_piece->is_white()) { //check if capturing piece is opponent's
        return false; 
    }

    if (!(start_piece->legal_capture_shape(start, end))) {//check if legal capture move
        return false; 
    }

    if (!(board.path_clear(start, end))) {//check if board path is clear
        return false; 
    }

    //if all conditions are met, piece can be moved
    return true;

}

bool Chess::in_check(bool white) const {
    //find king position
    pair <char,char> king_pos = board.get_king_position(white); 
    pair <char,char> opp_pos;     
    
    //find all opponent's pieces 
    vector<pair <char, char>> opp_pieces = board.all_color_pieces(!white);
    
    //iterate through opponent's pieces 
    for(vector<pair<char,char>>::iterator it = opp_pieces.begin();
        it != opp_pieces.end(); ++it) {
        opp_pos = *it;
        if (can_capture(opp_pos, king_pos)) { //test if any opponent piece can capture king
	    return true; 
        }
    }
    return false; 
}

//if in_check and after legal_move, still in_check
bool Chess::in_mate(bool white) const {
    if (in_check(white) && in_stalemate(white)) {
        return true;
    }
    return false;
}


bool Chess::in_stalemate(bool white) const {
    Board* in_board = const_cast<Board*> (&board); //const_cast pointer made to edit board directly 
    Board temp_board = this->get_board();  //temp board calls copy constructor, saves current board status
    
    std::pair<char, char> start_pos; 
    std::pair<char, char> end_pos; 
    
    //get all current player's pieces 
    vector<pair <char, char>> all_pieces = temp_board.all_color_pieces(white); 
    
    for(vector<pair<char,char>>::iterator it = all_pieces.begin(); it != all_pieces.end(); ++it) {
        start_pos = *it; 
        for (char r = '8'; r >= '1'; r--) {  //loop through board for valid moves
            for (char c = 'A'; c <= 'H'; c++) {
                end_pos = std::make_pair(c, r);
                if (can_move(start_pos, end_pos)) { //make move on current board
	                in_board->move_piece(start_pos, end_pos);
	        }
                else { //if move cannot be made, continue to next iteration
                    continue; 
                }
	 
                if (!in_check(white)) { //return false if any move does not cause check
                    copy_board(temp_board, *in_board); 
                    return false;
                } 
                else {
                    copy_board(temp_board, *in_board); //reset board before next iteration
                }
            }
        } 
    }

    copy_board(temp_board, *in_board); //resets board before exiting
    return true; //return true if all moves result in check
}


/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator<< (std::ostream& os, const Chess& chess) {
    // Write the board out and then either the character 'w' or the character 'b',
    // depending on whose turn it is
    return os << chess.get_board() << (chess.turn_white() ? 'w' : 'b');
}

std::istream& operator>> (std::istream& is, Chess& chess) {
    Board& board = chess.get_edit_board(); 
    board.clear_board(); 

    if (!is.fail()) { //checks for malformed or empty file
	char piece;
        for (char row = '8'; row >= '1'; row--) {
            for (char col = 'A'; col <= 'H'; col++) {
                is.get(piece);
                if (piece != '-') {
	            board.add_piece(std::pair<char, char>(col, row), piece);
                }
            }    
            is.get(piece); //acounts for extra space at end of row 
         }   

        char color;
        is.get(color);
        chess.set_turn(color);

	if (!board.has_valid_kings()) { //clear board reduces memory leak
            board.clear_board(); 
	}
    }
    
    return is;
}

void Chess::set_turn(char color) {
    if (color == 'b' || color == 'B') {
        is_white_turn = false;
    }

    if (color == 'w' || color == 'W') {
        is_white_turn = true;
    }
}

